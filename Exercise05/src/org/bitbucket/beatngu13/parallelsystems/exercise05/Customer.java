package org.bitbucket.beatngu13.parallelsystems.exercise05;

import java.util.concurrent.CountDownLatch;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;
import java.util.concurrent.ThreadLocalRandom;
import java.util.concurrent.TimeUnit;

public class Customer {

    public static final int NUMBER_OF_CUSTOMERS = 30;
    public static final long CUSTOMER_FREQUENCY = 3L;

    public static void main(String[] args) throws InterruptedException {
        ThreadLocalRandom rand = ThreadLocalRandom.current();
        ExecutorService pool = Executors.newCachedThreadPool();
        HilzersBarbershop shop = new HilzersBarbershop();

        for (int i = 1; i <= NUMBER_OF_CUSTOMERS; i++) {
            Customer customer = new Customer(i, rand.nextLong(3L, 7L), rand.nextLong(2L, 5L));

            pool.execute(() -> shop.enter(customer));
            TimeUnit.SECONDS.sleep(CUSTOMER_FREQUENCY);
        }

        pool.shutdown();
        shop.close();
    }

    private final CountDownLatch finished = new CountDownLatch(1);

    private final int id;
    private final long haircutDuration;
    private final long paymentDuration;

    public Customer(int id, long haircutDuration, long paymentDuration) {
        this.id = id;
        this.haircutDuration = haircutDuration;
        this.paymentDuration = paymentDuration;
    }

    public void awaitFinished() throws InterruptedException {
        System.out.println(toString() + " waits on sofa.");
        finished.await();
    }

    public void signalFinished() {
        System.out.println(toString() + " done and prepares to leave.");
        finished.countDown();
    }

    public void sitInBarberChair(String barberName) throws InterruptedException {
        System.out.println(toString() + " sits in barber chair of " + barberName + ".");
        TimeUnit.SECONDS.sleep(haircutDuration);
    }

    public void pay(String barberName) throws InterruptedException {
        System.out.println(toString() + " pays " + barberName + ".");
        TimeUnit.SECONDS.sleep(paymentDuration);
    }

    @Override
    public String toString() {
        return "Customer{id=" + id + "}";
    }

}
