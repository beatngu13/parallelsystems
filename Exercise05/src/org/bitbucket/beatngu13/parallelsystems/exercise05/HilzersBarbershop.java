package org.bitbucket.beatngu13.parallelsystems.exercise05;

import java.util.concurrent.ArrayBlockingQueue;
import java.util.concurrent.BlockingQueue;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;
import java.util.concurrent.Semaphore;
import java.util.concurrent.TimeUnit;
import java.util.concurrent.atomic.AtomicInteger;

/*
 * Taken from Allan B. Downey - The Little Book of Semaphores (version 2.1.5):
 *
 * Our barbershop has three chairs, three barbers, and a waiting area that can accommodate four customers on a sofa and
 * that has standing room for additional customers. Fire codes limit the total number of customers in the barbershop to 20.
 *
 * A customer will not enter the barbershop if it is filled to capacity with other customers. Once inside, the customer takes
 * a seat on the sofa or stands if the sofa is filled. When a barber is signalFinished, the customer that has been on the sofa the
 * longest is served and, if there are any standing customers, the one that has been in the barbershop the longest takes a
 * seat on the sofa. When a customer's haircut is finished, any barber can accept payment, but because there is only one
 * cash register, payment is accepted for one customer at a time. The barbers divide their time among cutting hair,
 * accepting payment, and sleeping in their chair waiting for a customer.
 */
public class HilzersBarbershop {

    public static final int SHOP_LIMIT = 20;
    public static final int SIZE_OF_SOFA = 4;
    public static final int NUMBER_OF_CASH_REGISTERS = 1;
    public static final int NUMBER_OF_BARBERS = 3;

    private Semaphore barbershop = new Semaphore(SHOP_LIMIT, true);
    private BlockingQueue<Customer> sofa = new ArrayBlockingQueue<>(SIZE_OF_SOFA, true);
    private Semaphore cashRegister = new Semaphore(NUMBER_OF_CASH_REGISTERS, true);

    private AtomicInteger barberIdProvider = new AtomicInteger();
    private ExecutorService barbers = Executors.newFixedThreadPool(NUMBER_OF_BARBERS,
            (barber) -> new Thread(barber, "Barber{id=" + barberIdProvider.incrementAndGet() + "}"));

    private boolean open = true;

    public HilzersBarbershop() {
        System.out.println("Opening Hilzer's barbershop.");

        for (int i = 0; i < NUMBER_OF_BARBERS; i++) {
            barbers.execute(() -> {
                String barberName = Thread.currentThread().getName();

                while (open || !sofa.isEmpty()) {
                    try {
                        Customer customer = sofa.poll(10L, TimeUnit.SECONDS);

                        // Barber#cutHair
                        if (customer != null) {
                            customer.sitInBarberChair(barberName);

                            // Barber#acceptPayment
                            try {
                                cashRegister.acquire();
                                System.out.println(barberName + " using cash register.");

                                customer.pay(barberName);
                            } finally {
                                cashRegister.release();
                                System.out.println(barberName + " leaving cash register.");

                                customer.signalFinished();
                            }
                        }
                    } catch (InterruptedException e) { /* NOP */ }
                }
            });
        }
    }

    public void enter(Customer customer) {
        if (open) {
            try {
                // Customer#enterShop
                barbershop.acquire();
                System.out.println(customer + " entered barbershop.");

                // Customer#sitOnSofa
                if (sofa.offer(customer, 1L, TimeUnit.MINUTES)) {
                    customer.awaitFinished();
                }
            } catch (InterruptedException e) {
                // NOP
            } finally {
                // Customer#exitShop
                barbershop.release();
                System.out.println(customer + " left barbershop.");
            }
        } else {
            System.out.println(customer + " rejected since barbershop is already closed.");
        }
    }

    public void close() {
        System.out.println("Closing Hilzer's barbershop.");
        open = false;
        barbers.shutdown();
    }

}
