package org.bitbucket.beatngu13.parallelsystems.exercise02;

import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;
import java.util.concurrent.ThreadLocalRandom;
import java.util.concurrent.TimeUnit;
import java.util.concurrent.locks.Condition;
import java.util.concurrent.locks.ReentrantLock;

public class Supermarket {

    private final ReentrantLock lock = new ReentrantLock();
    private final Condition condition = lock.newCondition();

    private int freeMachines = 3;

    public static void main(String[] args) {
        Supermarket supermarket = new Supermarket();
//        ExecutorService executor = Executors.newCachedThreadPool();
//        ExecutorService executor = Executors.newSingleThreadExecutor();
        ExecutorService executor = Executors.newFixedThreadPool(10);

        for (int i = 1; i <= 25; i++) {
            String customerId = Integer.toString(i);

            executor.execute(() -> {
                ThreadLocalRandom rand = ThreadLocalRandom.current();
                int baskets = rand.nextInt(2, 6);

                try {
                    System.out.println("Customer " + customerId + " enters supermarket with " + baskets + " baskets.");
                    supermarket.useMachine(customerId);

                    while (baskets-- > 0) {
                        long duration = rand.nextLong(3L, 7L);

                        TimeUnit.SECONDS.sleep(duration);
                        System.out.println("Customer " + customerId + " dumped a basket in " + duration
                                + "s (baskets left = " + baskets + ").");
                    }
                } catch (InterruptedException e) {
                    // NOP
                } finally {
                    supermarket.leaveMachine(customerId);
                }
            });

            try {
                TimeUnit.SECONDS.sleep(8L);
            } catch (InterruptedException e) { /* NOP */ }
        }

        executor.shutdown();
    }

    public void useMachine(String customerId) {
        try {
            lock.lock();

            while (freeMachines == 0) {
                System.out.println("Customer " + customerId + " waits (machines left = " + freeMachines + ").");
                condition.await();
            }

            freeMachines--;
            System.out.println("Customer " + customerId + " uses a machine (machines left = " + freeMachines + ").");
        } catch (InterruptedException e) {
            // NOP
        } finally {
            lock.unlock();
        }
    }

    public void leaveMachine(String customerId) {
        try {
            lock.lock();
            freeMachines++;
            System.out.println("Customer " + customerId + " leaves a machine (machines left = " + freeMachines + ").");
            condition.signal();
        } finally {
            lock.unlock();
        }
    }

}
