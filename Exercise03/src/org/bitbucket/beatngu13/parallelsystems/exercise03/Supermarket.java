package org.bitbucket.beatngu13.parallelsystems.exercise03;

import java.util.concurrent.*;

public class Supermarket {

    private static final int NUMBER_OF_CUSTOMERS = 25;

    private Semaphore semaphore = new Semaphore(3);

    public static void main(String[] args) {
        Supermarket supermarket = new Supermarket();
        ScheduledExecutorService scheduler = Executors.newSingleThreadScheduledExecutor();
        ExecutorService pool = Executors.newCachedThreadPool();
        CountDownLatch latch = new CountDownLatch(NUMBER_OF_CUSTOMERS);

        Runnable customer = () -> {
            String customerId = Long.toString(NUMBER_OF_CUSTOMERS - latch.getCount());
            ThreadLocalRandom rand = ThreadLocalRandom.current();
            int baskets = rand.nextInt(2, 6);

            try {
                System.out.println("Customer " + customerId + " enters supermarket with " + baskets + " baskets.");
                supermarket.useMachine(customerId);

                while (baskets-- > 0) {
                    long duration = rand.nextLong(3L, 7L);

                    TimeUnit.SECONDS.sleep(duration);
                    System.out.println("Customer " + customerId + " dumped a basket in " + duration
                            + "s (baskets left = " + baskets + ").");
                }
            } catch (InterruptedException e) {
                // NOP
            } finally {
                supermarket.leaveMachine(customerId);
            }
        };

        scheduler.scheduleAtFixedRate(() -> {
            latch.countDown();
            pool.execute(customer);
        }, 0L, 7L, TimeUnit.SECONDS);

        try {
            latch.await();
            scheduler.shutdown();
            pool.shutdown();
        } catch (InterruptedException e) { /* NOP */ }
    }

    public void useMachine(String customerId) throws InterruptedException {
        semaphore.acquire();
        System.out.println("Customer " + customerId + " uses a machine (machines left = "
                + semaphore.availablePermits() + ").");
    }

    public void leaveMachine(String customerId) {
        semaphore.release();
        System.out.println("Customer " + customerId + " leaves a machine (machines left = "
                + semaphore.availablePermits() + ").");
    }

}
