package org.bitbucket.beatngu13.parallelsystems.exercise04;

import java.util.Comparator;
import java.util.concurrent.BlockingQueue;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;
import java.util.concurrent.PriorityBlockingQueue;
import java.util.concurrent.TimeUnit;
import java.util.concurrent.atomic.AtomicInteger;

public class Supermarket {

    private AtomicInteger machinesIdProvider = new AtomicInteger();
    private ExecutorService machines = Executors.newFixedThreadPool(Setup.NUMBER_OF_MACHINES,
            (runnable) -> new Thread(runnable, "machine " + machinesIdProvider.incrementAndGet())); // Exception in pool iff id > number of machines.
    private boolean open = true;
    private BlockingQueue<Customer> customers = new PriorityBlockingQueue<>(Setup.NUMBER_OF_CUSTOMERS,
            Comparator.comparing(Customer::getType).thenComparing(Customer::getId)); // Bloch doesn't like.

    public Supermarket() {
        System.out.println("Opening supermarket.");
        for (int i = 0; i < Setup.NUMBER_OF_MACHINES; i++) {
            machines.execute(() -> {
                String machineName = Thread.currentThread().getName();
                while (open || !customers.isEmpty()) {
                    try {
                        customers.poll(30L, TimeUnit.SECONDS).dumpBasketsAndLeave(machineName);
                    } catch (InterruptedException e) { /* NOP */ }
                }
            });
        }
    }

    public void enter(Customer customer) {
        try {
            if (open) {
                System.out.println(customer + " enters supermarket and enqueues.");
                customers.put(customer);
            } else {
                System.out.println(customer + " rejected since supermarket is already closed.");
            }
        } catch (InterruptedException e) { /* NOP */ }
    }

    public void close() {
        System.out.println("Closing supermarket.");
        open = false;
        machines.shutdown();
    }

}
