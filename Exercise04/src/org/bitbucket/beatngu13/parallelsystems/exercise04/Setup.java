package org.bitbucket.beatngu13.parallelsystems.exercise04;

import java.util.concurrent.CountDownLatch;
import java.util.concurrent.Executors;
import java.util.concurrent.ScheduledExecutorService;
import java.util.concurrent.TimeUnit;
import java.util.function.Consumer;

import static org.bitbucket.beatngu13.parallelsystems.exercise04.Customer.CustomerType;

public class Setup {

    public static final int NUMBER_OF_MACHINES = 3;
    public static final int NUMBER_OF_CUSTOMERS = 25;

    public static void main(String[] args) {
        ScheduledExecutorService customerScheduler = Executors.newSingleThreadScheduledExecutor();
        CountDownLatch latch = new CountDownLatch(NUMBER_OF_CUSTOMERS);
        Supermarket supermarket = new Supermarket();

        Consumer<CustomerType> createAndEnter = (type) -> {
            synchronized (latch) {
                if (latch.getCount() > 0) {
                    latch.countDown();
                    supermarket.enter(new Customer(type, NUMBER_OF_CUSTOMERS - (int) latch.getCount()));
                }
            }
        };

        customerScheduler.scheduleAtFixedRate(() -> createAndEnter.accept(CustomerType.GOLD), 0L, 10L, TimeUnit.SECONDS);
        customerScheduler.scheduleAtFixedRate(() -> createAndEnter.accept(CustomerType.CASUAL), 0L, 7L, TimeUnit.SECONDS);

        try {
            latch.await();
            customerScheduler.shutdown();
            supermarket.close();
        } catch (InterruptedException e) { /* NOP */ }
    }

}
