package org.bitbucket.beatngu13.parallelsystems.exercise04;

import java.util.concurrent.ThreadLocalRandom;
import java.util.concurrent.TimeUnit;

public class Customer {

    public enum CustomerType {
        GOLD,
        CASUAL
    }

    private final CustomerType type;
    private final int id;

    private int baskets;

    public Customer(CustomerType type, int id) {
        this.type = type;
        this.id = id;
        baskets = ThreadLocalRandom.current().nextInt(2, 6);
    }

    public void dumpBasketsAndLeave(String machineName) {
        try {
            System.out.println(toString() + " dequeues and starts using " + machineName + ".");
            long customerDuration = 0L;

            while (baskets > 0) {
                long basketDuration = ThreadLocalRandom.current().nextLong(3L, 7L);
                customerDuration += basketDuration;
                baskets--;

                TimeUnit.SECONDS.sleep(basketDuration);
                System.out.println(toString() + " dumped a basket in " + basketDuration + " s.");
            }

            System.out.println(toString() + " frees " + machineName + " after " + customerDuration + " s and leaves supermarket.");
        } catch (InterruptedException e) { /* NOP */ }
    }

    public CustomerType getType() {
        return type;
    }

    public int getId() {
        return id;
    }

    @Override
    public String toString() {
        return "Customer " + id
                + " (type = " + type.toString().toLowerCase()
                + ", baskets = " + baskets + ")";
    }

}
