package org.bitbucket.beatngu13.parallelsystems.exercise01;

import java.util.concurrent.ThreadLocalRandom;
import java.util.concurrent.TimeUnit;

public class Supermarket {

    private int freeMachines = 3;

    public static void main(String[] args) {
        Supermarket supermarket = new Supermarket();

        for (int i = 1; i <= 25; i++) {
            String customerId = Integer.toString(i);

            new Thread(() -> {
                ThreadLocalRandom rand = ThreadLocalRandom.current();
                int baskets = rand.nextInt(2, 6);

                try {
                    System.out.println("Customer " + customerId + " enters supermarket with " + baskets + " baskets.");
                    supermarket.useMachine(customerId);

                    while (baskets-- > 0) {
                        long duration = rand.nextLong(3L, 7L);

                        TimeUnit.SECONDS.sleep(duration);
                        System.out.println("Customer " + customerId + " dumped a basket in " + duration
                                + "s (baskets left = " + baskets + ").");
                    }
                } catch (InterruptedException e) {
                    // NOP
                } finally {
                    supermarket.leaveMachine(customerId);
                }
            }, customerId).start();

            try {
                TimeUnit.SECONDS.sleep(8L);
            } catch (InterruptedException e) { /* NOP */ }
        }
    }

    public synchronized void useMachine(String customerId) {
        while (freeMachines == 0) {
            try {
                System.out.println("Customer " + customerId + " waits (machines left = " + freeMachines + ").");
                wait();
            } catch (InterruptedException e) { /* NOP */ }
        }

        freeMachines--;
        System.out.println("Customer " + customerId + " uses a machine (machines left = " + freeMachines + ").");
    }

    public synchronized void leaveMachine(String customerId) {
        freeMachines++;
        System.out.println("Customer " + customerId + " leaves a machine (machines left = " + freeMachines + ").");
        notify();
    }

}
